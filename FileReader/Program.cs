﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FileReader.Entities;

namespace FileReader
{
    class Program
    {
        static void Main(string[] args)
        {
            Run();
        }

        private static void Run()
        {
            using (FileSystemWatcher watcher = new FileSystemWatcher())
            {
                watcher.Path = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\data\\in";

                watcher.NotifyFilter = NotifyFilters.LastAccess
                                     | NotifyFilters.LastWrite
                                     | NotifyFilters.FileName
                                     | NotifyFilters.DirectoryName;

                watcher.Filter = "*.dat";

                watcher.Changed += OnChanged;
                watcher.Created += OnChanged;
                watcher.Deleted += OnChanged;

                watcher.EnableRaisingEvents = true;

               Console.Read();
            }
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            List<Customer> customers = new List<Customer>();
            List<Salesperson> salespeople = new List<Salesperson>();
            List<Sale> sales = new List<Sale>();

            string fileContent = File.ReadAllText(e.FullPath, Encoding.UTF8);

            string[] lines = fileContent.Split(
                new[] { Environment.NewLine },
                StringSplitOptions.None
            );

            string[] aux;
            string[] itemsAux;
            string[] itemAux;

            foreach (string line in lines)
            {
                aux = line.Split("ç");
                if (aux[0] == "001") salespeople.Add(new Salesperson() { CNPJ = aux[1], Name = aux[2], BusinessArea = aux[3] });
                else if (aux[0] == "002") customers.Add(new Customer() { CPF = aux[1], Name = aux[2], Salary = aux[3] }); 
                else if (aux[0] == "003")
                {
                    itemsAux = aux[2].Replace("[", string.Empty).Replace("]", string.Empty).Split(",");
                    List<Item> items = new List<Item>();
                    foreach (string item in itemsAux)
                    {
                        itemAux = item.Split("-");
                        items.Add(new Item() { ID = Int32.Parse(itemAux[0]), Quantity = int.Parse(itemAux[1]), Price = decimal.Parse(itemAux[2]) });
                    }
                    sales.Add(new Sale() { ID = Int32.Parse(aux[1]), Items = items, SalespersonName = aux[3] });
                }
            }

            string response = String.Format("{0},{1},{2},{3}", customers.Count, salespeople.Count, GetPriciestSale(sales), GetWorstSalesperson(sales, salespeople));

            string fileName = e.Name.ToLower().Replace(" ", "_") + ".done.dat";
            File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\data\\out\\" + fileName, response);
        }

        public static int GetPriciestSale(List<Sale> sales)
        {
            int priciestSale = 0;
            decimal priciestSalePrice = 0;
            decimal salePrice = 0;

            foreach (Sale sale in sales)
            {
                foreach (Item item in sale.Items)
                {
                    salePrice += item.Price * item.Quantity;
                }

                priciestSale = priciestSalePrice > salePrice ? priciestSale : sale.ID;
                priciestSalePrice = priciestSalePrice > salePrice ? priciestSalePrice : salePrice;
                salePrice = 0;
            }

            return priciestSale;
        }

        public static string GetWorstSalesperson(List<Sale> sales, List<Salesperson> salespeople)
        {
            Salesperson worstSalesperson = salespeople.FirstOrDefault();
            int worstNumberOfSales = Int32.MaxValue;

            foreach (Salesperson salesperson in salespeople)
            {
                worstSalesperson = (sales.Where(x => x.SalespersonName == salesperson.Name).Count() < worstNumberOfSales) ? salesperson : worstSalesperson;
                worstNumberOfSales = (sales.Where(x => x.SalespersonName == salesperson.Name).Count() < worstNumberOfSales) ? sales.Where(x => x.SalespersonName == worstSalesperson.Name).Count() : worstNumberOfSales;
            }

            return worstSalesperson.Name;
        }
    }
}
