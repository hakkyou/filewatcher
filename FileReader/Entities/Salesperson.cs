﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileReader.Entities
{
    public class Salesperson
    {
        public string CNPJ { get; set; }
        public string Name { get; set; }
        public string BusinessArea { get; set; }
    }
}
