﻿using FileReader.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FileReader.Entities
{
    public class Sale
    {
        public int ID { get; set; }
        public List<Item> Items { get; set; }
        public string SalespersonName { get; set; }
    }
}
